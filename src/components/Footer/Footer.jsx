import React from 'react'
import { NavLink } from 'react-router-dom'
import style from './Footer.module.css'
const Footer = () => {
  return (
    <div>
        <div className={style.about}>
            <div className={style.liste}>
                <h2>Product</h2>
                <ul>
                    <li>
                        <NavLink to="/" className="navActive">
                            <span>Email Row</span> 
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/" className="navActive">
                            <span>Free Tools</span> 
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/" className="navActive">
                            <span>Agents</span> 
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/" className="navActive">
                            <span>Blog</span> 
                        </NavLink>
                    </li>
                </ul>
            </div>
            <div className={style.liste}>
                <h2>Resources</h2>
                <ul>
                    <li>
                        <NavLink to="/" className="navActive">
                            <span>Our Agents</span> 
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/" className="navActive">
                            <span>Member Stories </span> 
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/" className="navActive">
                            <span>Video</span> 
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/" className="navActive">
                            <span>Free trial </span> 
                        </NavLink>
                    </li>
                </ul>
            </div>
            <div className={style.liste}>
                <h2>Company</h2>
                <ul>
                    <li>
                        <NavLink to="/" className="navActive">
                            <span>Patnerships </span> 
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/" className="navActive">
                            <span>Terms of use</span> 
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/" className="navActive">
                            <span>Privacy</span> 
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/" className="navActive">
                            <span>Sitemap </span> 
                        </NavLink>
                    </li>
                </ul>
            </div>
            <div className={style.liste}>
                <h2>Get In Touch</h2>
                <p>you'll find your next marketing value your prefer</p>
                <ul className={style.underlist}>
                    <li>
                        <NavLink to="/" className={style.facebook}>
                            <span></span> 
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/" className={style.twitter}>
                            <span></span> 
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/" className={style.linkedin}>
                            <span></span> 
                        </NavLink>
                    </li>
                </ul>
            </div>
        </div>
        <div className={style.Footer}>
            <p>CopyRight2023j-shop.com,all rights reserved</p>
        </div>
    </div>
  )
}

export default Footer