import React from 'react'
import style from './Container.module.css'
import { NavLink } from 'react-router-dom'
import Vector from '../../images/Vector.png'
import facebook from '../../images/facebook.png'
import linkedin from '../../images/linkedin.png'
import twitter from '../../images/twitter.png'
import bague1 from '../../images/bague1.png'
import play from '../../images/play.png'
import portrait from '../../images/portrait.png'
import portrait2 from '../../images/portrait2.png'
import bracelet from '../../images/bracelet.png'
import cercle from '../../images/cercle.png'
import bague2 from '../../images/bague2.png'
import bague3 from '../../images/bague3.png'
import bague4 from '../../images/bague4.png'
const Container = () => {
  return (
    <div>
        <div className={style.container}>
            <div className={style.gauche}>
                <h1>Discover The <br /> Exceptional Jewellery <br /> With Us</h1>
                <p>
                    Lorem ipsum dolor sit amet. Qui consequatur sint 33 voluptatem officia et <br /> sint laboriosam sed ipsa sin
                </p>
                <div className={style.button}>
                    <button> GET STARTED</button>
                    <NavLink>
                        <img src={play} alt="" />
                        <span>Play Video</span>
                    </NavLink>
                </div>
            </div>
            <div className={style.circle}>
                <NavLink>Explore</NavLink>
            </div>
            <div className={style.centre}>
                <img src={bague1} alt="" />
            </div>
            <div className={style.droite}>
                <NavLink><img src={twitter}alt=""/></NavLink>
                <NavLink><img src={Vector} alt="" /></NavLink>
                <NavLink><img src={linkedin} alt="" /></NavLink>
                <NavLink><img src={facebook} alt="" /></NavLink>
            </div>
        </div>
        <div className={style.container}>
            <div className={style.centre}>
                <img src={bague2} alt="" />
            </div>
            <div className={style.gauche}>
                <div>
                    <h1>Silver Round Designer <br /> Bracelet For Women </h1>
                    <p>
                        Lorem ipsum dolor sit amet. Qui consequatur sint 33 voluptatem officia et sint <br /> laboriosam sed ipsa sint ut voluptatum labore et possimus voluptas. Vel vitae <br /> temporibus sit nulla consequatur in illo galisum eo
                    </p>
                    <div className={style.button}>
                        <button> ADD TO CART</button>
                        <span>$549.29</span>
                    </div>
                </div>
                
            </div>
            
        </div>
        <div className={style.Cgauche}>
            <div className={style.container}>
                <div>
                    <img src={bague3} alt="" />
                </div>
                <div>
                    <h1>Our Story</h1>
                    <p>modern jewelry is made of gold, silver, or platinum, <br /> often with precious or semiprecious stones, it evolved from <br /> shells, animal teeth, and other items used as <br /> body decoration in prehistoric times.</p>
                </div>
            </div>
        </div>
        <div className={style.container}>
            <div className={style.gauche}>
                <h1>Silver Round Designer <br /> Bracelet For Women </h1>
                <p>
                    Lorem ipsum dolor sit amet. Qui consequatur sint 33 voluptatem officia et sint <br /> laboriosam sed ipsa sint ut voluptatum labore et possimus voluptas. Vel vitae <br /> temporibus sit nulla consequatur in illo galisum eo
                </p>
                <div >
                    <h2>Size</h2>
                    <div className={style.size}>
                        <span className={style.Scircle}>S</span>
                        <span>M</span>
                        <span>L</span>
                    </div>
                </div>
                <div className={style.button}>
                    <button> ADD TO CART</button>
                    <span>$549.29</span>
                </div>
            </div>
            <div className={style.centre}>
                <img src={bracelet} alt="" />
            </div>
        </div>
        <div className={style.container}>
            <div className={style.centre}>
                <img src={bague4} alt="" />
            </div>
            <div className={style.gauche}>
                <h1>An Exquisite Diamond <br /> Jewellery  </h1>
                <p>
                    Lorem ipsum dolor sit amet. Qui consequatur sint 33 voluptatem officia et sint <br /> laboriosam sed ipsa sint ut voluptatum labore et possimus voluptas. Vel vitae <br /> temporibus sit nulla consequatur in illo galisum eo
                </p>
                <div className={style.button}>
                    <button> VIEW MORE</button>
                    <span>$689.49</span>
                </div>
            </div> 
        </div>

        <div className={style.container}>
            <div className={style.gauche}>
                <h1>Jewell Best Collection <br /> For You All</h1>
                <p>
                    Lorem ipsum dolor sit amet. Qui consequatur sint 33 voluptatem officia et sint <br /> laboriosam sed ipsa sint ut voluptatum labore et possimus voluptas. Vel vitae <br /> temporibus sit nulla consequatur in illo galisum eo
                </p>
                <div className={style.button}>
                    <button>VIEW MORE</button>
                </div>
            </div>
            <div className={style.centre}>
                <img src={portrait} alt="" />
            </div>
        </div>
        <div className={style.container}>
            <div className={style.centre}>
                <img src={portrait2} alt="" />
            </div>
            <div className={style.medaille}>
                <img src={cercle} alt="" />
            </div>
            <div className={style.gauche}>
                <h1>We Have Everything <br /> Which Trendy </h1>
                <p>
                    Lorem ipsum dolor sit amet. Qui consequatur sint 33 voluptatem officia et sint <br /> laboriosam sed ipsa sint ut voluptatum labore et possimus voluptas. Vel vitae <br /> temporibus sit nulla consequatur in illo galisum eo
                </p>
                <div className={style.button}>
                    <button>VIEW MORE</button>
                </div>
            </div>
        </div>

    </div>
  )
}

export default Container