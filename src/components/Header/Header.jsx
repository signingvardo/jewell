import React from 'react'
import { NavLink } from 'react-router-dom'
import style from './Header.module.css'
const Header = () => {
  return (
    <div className={style.header}>
      <div className={style.logo}>
        <p>Jewell</p>
      </div>
      <div className={style.navigation}>
        <ul>
          <li>
            <NavLink to="/" className="navActive">
              <span>Home</span> 
            </NavLink>
          </li>
          <li>
            <NavLink to="/" className="navActive">
              <span>About</span> 
            </NavLink>
          </li>
          <li>
            <NavLink to="/" className="navActive">
              <span>Services</span> 
            </NavLink>
          </li>
          <li>
            <NavLink to="/" className="navActive">
              <span>Catalogue</span> 
            </NavLink>
          </li>
          <li>
            <NavLink to="/" className={style.menu}>
               
            </NavLink>
          </li>
        </ul>
        <div className={style.button}>
          <ul>
            <li className={style.first}>
              <NavLink to="/" >
                <button>Log In</button>
              </NavLink>
            </li>
            <li className={style.last}>
              <NavLink to="/">
                <button>Get Started</button>
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
      
    </div>
  )
}

export default Header