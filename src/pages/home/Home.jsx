import React from 'react'
import Container from '../../components/container/Container'
import Footer from '../../components/Footer/Footer'
import Header from '../../components/Header/Header'
const Accueil = () => {
  return (
    <div className='accueil'>
      <Header/>
      <Container/>
      <Footer/>
    </div>
  )
}

export default Accueil