import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Home from './pages/home/Home'
import Catalogue from './pages/catalogue/Catalogue'
import About from './pages/about/About'
import PageNotFound from './pages/pagenotfound/PageNotFound'
import Services from './pages/offre/Service'
import React from 'react'

const App = () => {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Home/>}/>
          <Route path='/catalogue' element={<Catalogue/>} />
          <Route path='/about' element={<About/>} />
          <Route path='/services' element={<Services/>} />
          <Route path='*' element={<PageNotFound/>} />
        </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App